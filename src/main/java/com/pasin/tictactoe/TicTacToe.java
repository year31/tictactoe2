/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Pla
 */
public class TicTacToe {

    static Scanner kb = new Scanner(System.in);
    static char currentPlayer = 'O';
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static int row, col;
    static boolean finish = false;
    static int trun = 0;
    static boolean canPut = false;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurnPlayer();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("welcome to OX Game");
    }

    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + "    ");
            }
            System.out.println("\n");
        }
    }

    public static void showTurnPlayer() {
        System.out.println("Turn    " + currentPlayer);
    }

    public static void inputRowCol() {
        canPut=false;
        while (canPut==false) {
            showTextInPutRowCol();
            row = kb.nextInt();
            col = kb.nextInt();
            cheackSlot();
        }

    }

    public static void showTextInPutRowCol() {
        System.out.println("Please input row and col ");
    }

    public static void process() {
        if (setTable()) {
            if (cheackWin()) {
                finish = true;
                showTable();
                showWin();
                return;
            }
            if (cheackDraw()) {
                finish = true;
                showTable();
                showDraw();
                return;
            }
            trun++;
            switchPlayer();
        }

    }

    public static void switchPlayer() { // switch Player O to X and X to O
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() { // put currentPlayer to table
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean cheackWin() {
        if (cheackRow()) {
            return true;
        } else if (cheackCol()) {
            return true;
        } else if (cheackX()) {
            return true;
        }
        return false;

    }

    public static boolean cheackCol() {
        for (int j = 0; j < 3; j++) { // cheack win 
            if (table[0][j] == table[1][j] && table[1][j] == table[2][j] && table[0][j] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean cheackRow() {
        for (int i = 0; i < 3; i++) { // cheack win 
            if (table[i][0] == table[i][1] && table[i][1] == table[i][2] && table[i][0] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean cheackX() {
        if (cheackX1()) {
            return true;
        } else if (cheackX2()) {
            return true;
        }
        return false;
    }

    public static boolean cheackX1() { // cheack win condition \
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
            return true;
        }
        return false;
    }

    public static boolean cheackX2() { // cheack win comdition /
        if (table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[0][2] != '-') {
            return true;
        }
        return false;
    }

    public static boolean cheackDraw() {
        if (trun >= 8) {
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>> Draw <<<");
    }

    public static void showWin() {
        System.out.println(">>> " + currentPlayer + " Win <<<");
    }

    public static boolean slotEmpty() {
        if (table[row-1][col-1]=='-') {
            return true;
        }
        showSlotNotEmpty();
        return false;
    }

    public static boolean cheackSlot() {
        if (slotInTable()) {
            if (slotEmpty()) {
                return canPut=true;
            }
        }
        return canPut= false;
    }

    public static boolean slotInTable() {
        if (row > 0 && row <= 3 && col > 0 && col <= 3) {
            return true;
        }
        showOutOfTable();
        return false;
    }

    public static void showOutOfTable() {
        System.out.println("You out of table, Please input 1-3 in row and col");
    }

    public static void showSlotNotEmpty() {
        System.out.println("This slot have another character, Please input another slot");
    }

}
